# cstate-pusher
Simple script to send status notifications to CSTATE, XMPP and Fediverse. Script requires slixmpp python package and perhaps other that aren't included by default (note: need to check what is needed to be loaded and either list it or create requirenments file)

To configure, you need to add example.conf to `~/.config/cstate-pusher.conf` and fill in needed credentials. If you use pleroma/akkoma there is no straight forward way to obtain token. Follow those steps:
`curl -s -S -i -X POST -F client_name="Cstate pusher" -F redirect_uris="urn:ietf:wg:oauth:2.0:oob" -F scopes="read write" https://social.example.com/api/v1/apps`
This will give you response with `client_id and` `client_secret`.
Now run this:
`curl -s -S -i -X POST -F client_id="<client_id>" -F client_secret="<secret>" redirect_uri="urn:ietf:wg:oauth:2.0:oob" -F scope="read,write" -F grant_type="password" -F username="<account_username>" -F password="<account_password>" https://social.example.com/oauth/token`

Copy `access_token` to your `cstate-pusher.conf` file under `[Fedi][token]` section.


